#!/bin/bash

if [[ -z "$2" ]] ; then
    echo ""
    echo "Rules:"
    echo "  1) run inside asterisk-g72x folder"
    echo "  2) provide Yate build folder (yate-5.5.0-1) as first argument"
    echo "  3) provide Yate codec folder (yate-g72x) as second argument"
    echo ""
    echo "Example:"
    echo "    cd /path/to/asterisk-g72x"
    echo "    ./build_with_ipp7.sh /path/to/yate-5.5.0-1 /path/to/yate-g72x"
    echo ""
  exit
fi

YATE_BUILD_DIR="$1"
YATE_CODEC_DIR="$2"

#
# G.723
#

echo "Build G.723 Yate object $YATE_CODEC_DIR/g723codec.cpp -> .libs/codec_g723_la-codec_g72x.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I$YATE_BUILD_DIR -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_3 -O3 -fomit-frame-pointer -march=native -flto -MT codec_g723_la-codec_g72x.lo -MD -MP -MF .deps/codec_g723_la-codec_g72x.Tpo -c $YATE_CODEC_DIR/g723codec.cpp  -fPIC -DPIC -o .libs/codec_g723_la-codec_g72x.o

echo "Build G.723 decoder ipp/decg723.c -> ipp/.libs/codec_g723_la-decg723.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_3 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g723_la-decg723.lo -MD -MP -MF ipp/.deps/codec_g723_la-decg723.Tpo -c ipp/decg723.c  -fPIC -DPIC -o ipp/.libs/codec_g723_la-decg723.o

echo "Build G.723 encoder ipp/encg723.c -> ipp/.libs/codec_g723_la-encg723.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_3 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g723_la-encg723.lo -MD -MP -MF ipp/.deps/codec_g723_la-encg723.Tpo -c ipp/encg723.c  -fPIC -DPIC -o ipp/.libs/codec_g723_la-encg723.o

echo "Build G.723 internal ipp/owng723.c -> ipp/.libs/codec_g723_la-owng723.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_3 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g723_la-owng723.lo -MD -MP -MF ipp/.deps/codec_g723_la-owng723.Tpo -c ipp/owng723.c  -fPIC -DPIC -o ipp/.libs/codec_g723_la-owng723.o

echo "Build G.723 voice activity detect ipp/vadg723.c -> ipp/.libs/codec_g723_la-vadg723.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_3 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g723_la-vadg723.lo -MD -MP -MF ipp/.deps/codec_g723_la-vadg723.Tpo -c ipp/vadg723.c  -fPIC -DPIC -o ipp/.libs/codec_g723_la-vadg723.o

echo "Build G.723 aux tables ipp/aux_tbls.c -> ipp/.libs/codec_g723_la-aux_tbls.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_3 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g723_la-aux_tbls.lo -MD -MP -MF ipp/.deps/codec_g723_la-aux_tbls.Tpo -c ipp/aux_tbls.c  -fPIC -DPIC -o ipp/.libs/codec_g723_la-aux_tbls.o

echo "Create G.723 shared object Yate module (objects) -> $YATE_BUILD_DIR/modules/g723codec.yate"
gcc -shared  -fPIC -DPIC  .libs/codec_g723_la-codec_g72x.o ipp/.libs/codec_g723_la-decg723.o ipp/.libs/codec_g723_la-encg723.o ipp/.libs/codec_g723_la-owng723.o ipp/.libs/codec_g723_la-vadg723.o ipp/.libs/codec_g723_la-aux_tbls.o   -L/opt/intel/ipp/lib/intel64 -lippsc_l -lipps_l -lippcore_l  -O3 -march=native -flto   -Wl,-soname -Wl,codec_g723.so -o $YATE_BUILD_DIR/modules/g723codec.yate

#
# G.729
#

echo "Build G.729 Yate object $YATE_CODEC_DIR/g729codec.cpp -> .libs/codec_g729_la-codec_g72x.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I$YATE_BUILD_DIR -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_9 -O3 -fomit-frame-pointer -march=native -flto -MT codec_g729_la-codec_g72x.lo -MD -MP -MF .deps/codec_g729_la-codec_g72x.Tpo -c $YATE_CODEC_DIR/g729codec.cpp  -fPIC -DPIC -o .libs/codec_g729_la-codec_g72x.o

echo "Build G.729 decoder ipp/decg729fp.c -> ipp/.libs/codec_g729_la-decg729fp.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_9 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g729_la-decg729fp.lo -MD -MP -MF ipp/.deps/codec_g729_la-decg729fp.Tpo -c ipp/decg729fp.c  -fPIC -DPIC -o ipp/.libs/codec_g729_la-decg729fp.o

echo "Build G.729 encoder ipp/encg729fp.c -> ipp/.libs/codec_g729_la-encg729fp.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_9 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g729_la-encg729fp.lo -MD -MP -MF ipp/.deps/codec_g729_la-encg729fp.Tpo -c ipp/encg729fp.c  -fPIC -DPIC -o ipp/.libs/codec_g729_la-encg729fp.o

echo "Build G.729 internal ipp/owng729fp.c -> ipp/.libs/codec_g729_la-owng729fp.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_9 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g729_la-owng729fp.lo -MD -MP -MF ipp/.deps/codec_g729_la-owng729fp.Tpo -c ipp/owng729fp.c  -fPIC -DPIC -o ipp/.libs/codec_g729_la-owng729fp.o

echo "Build G.729 voice activity detect ipp/vadg729fp.c -> ipp/.libs/codec_g729_la-vadg729fp.o"
gcc -DHAVE_CONFIG_H -I. -DG72X_ASTERISK=130 -I/opt/intel/ipp/include -include /opt/intel/ipp/tools/intel64/staticlib/ipp_e9.h -Wall -D_GNU_SOURCE -Iipp -DG72X_9 -O3 -fomit-frame-pointer -march=native -flto -MT ipp/codec_g729_la-vadg729fp.lo -MD -MP -MF ipp/.deps/codec_g729_la-vadg729fp.Tpo -c ipp/vadg729fp.c  -fPIC -DPIC -o ipp/.libs/codec_g729_la-vadg729fp.o

echo "Create G.729 shared object Yate module (objects) -> $YATE_BUILD_DIR/modules/g729codec.yate"
gcc -shared  -fPIC -DPIC  .libs/codec_g729_la-codec_g72x.o ipp/.libs/codec_g729_la-decg729fp.o ipp/.libs/codec_g729_la-encg729fp.o ipp/.libs/codec_g729_la-owng729fp.o ipp/.libs/codec_g729_la-vadg729fp.o   -L/opt/intel/ipp/lib/intel64 -lippsc_l -lipps_l -lippcore_l  -O3 -march=native -flto   -Wl,-soname -Wl,codec_g729.so -o $YATE_BUILD_DIR/modules/g729codec.yate
