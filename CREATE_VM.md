
# Notes on how VirtualBox VM image for codec building was created

## VM

Install VirtualBox.

Create VM with Ubuntu Server 14.04, 2 GB RAM, 30 GB disk, no LVM.
Username: `builder`, password: `1234`.

Basic devbox setup:

```bash
sudo apt update
sudo apt upgrade

sudo apt install openssh-server
# add "UseDNS no" to /etc/ssh/sshd_config for faster logins

sudo apt install build-essential autoconf libtool
```

Configure any extra network interfaces in /etc/network/interfaces (this has to
be done if enabling more than one network adapter in VirtualBox settings). Then
do (assuming `eth1` is the new interface):

```bash
sudo ifup eth1
```

Set up host-guest shared folders by choosing `Devices -> Install Guest Additions CD...`;
then mount & run:

```bash
sudo mount /dev/cdrom /media/cdrom
sudo /media/cdrom/VBoxLinuxAdditions.run
```


## IPP

Find IPP 7.1 version in the Intel site
[here][https://registrationcenter.intel.com/en/products/]. Install as
evaluate/trial, without serial number. Older versions are only available to
those who signed up for them when they were still fresh. So really right now
only IPP 2017 may be available.

There's an unresolved issue with IPP v8.2 (and possibly others) where
`<ippsc.h>` header is not found. All the IPP versions are VERY different, so be
wary of that.

The latest 2017 version works as well but requires some extra installation steps
(see Arkadi's notes).

```bash
# download...
# extract...
./install.sh
# wait...
```


## Yate

```bash
# to support GSM and Speex codecs
sudo apt install libspeex-dev libgsm1-dev

# or any other version of course
wget http://yate.null.ro/tarballs/yate5/yate-5.5.0-1.tar.gz
tar -xf yate-5.5.0-1.tar.gz
mv yate yate-5.5.0-1
cd yate-5.5.0-1
./autogen.sh
make
```


## Codecs

Note: do not install `asterisk-dev` package! For some reason using that produces
strange compiler errors (although it did work before, so could work yet again).

```bash
# install Asterisk required packages
sudo apt install git mercurial uuid-dev libjansson-dev libxml2-dev libsqlite3-dev

# get Asterisk version 13 (other versions should work just as well)
git clone -b 13 http://gerrit.asterisk.org/asterisk asterisk-13

# build Asterisk
cd asterisk-13
./configure
make
```

Get Arkadi's Asterisk codec project & build it.

```bash
hg clone https://atis@bitbucket.org/arkadi/asterisk-g72x
cd asterisk-g72x
./autogen.sh
./configure --with-asterisk-includes=/home/builder/asterisk-13/include --with-asterisk130
make
```

Observe output from the above build process to create the `build_with_ipp*.sh`
utility scripts.

Get Arkadi's Yate codec project (do not build).

```bash
hg clone https://atis@bitbucket.org/arkadi/yate-g72x
```


## Final steps

Get `codec_builder` project and use it.

```bash
git clone git@bitbucket.org:datatechlabs/codec_builder.git
cd /path/to/asterisk-g72x
../codec_builder/build_with_ipp7.sh ../yate-5.5.0-1 ../yate-g72x

#
# DONE!
#
```

The codecs now exist in Yate build folder along with the built Yate itself.
Package it up and use it somewhere.

```bash
# install & run yate (install the new g72x modules too)
cd yate-5.5.0-1
sudo make install
sudo cp modules/g72* /usr/local/lib/yate/
LD_LIBRARY_PATH=/usr/local/lib yate -vvvv

# create archive
tar -czf yate_g72x_5.5.0-1.tgz yate-5.5.0-1
```


### G.723 send rate note

G.723 codec has two possible send rates (53 or 63). These can be configured by
setting the value of G723_RATE_63 in g723codec.cpp and recompiling.
